const path = require('path')
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

// connecting to db
mongoose.connect('mongodb://localhost/crud-mongo')
    .then(db => console.log('Db connected'))
    .catch(err => console.log(err));



// Importing routes
const indexRouts = require('./routes/index')

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'))
//app.set('views', __dirname + '/views') //Funciona sin requerir pero no funcionaria la ruta en Windows por la contrabarra. 
app.set('view engine', 'ejs');


// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }))

// routes
app.use('/', indexRouts)




// Startgin the server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})